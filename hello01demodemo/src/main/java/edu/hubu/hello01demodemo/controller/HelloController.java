package edu.hubu.hello01demodemo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @GetMapping("/hello")
    public String hello(){
        return "hello Jenkins!<br>软件工程1902班<br>46<br>张天桃";
    }
}
