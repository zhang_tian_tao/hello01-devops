package edu.hubu.hello01demodemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Hello01demodemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(Hello01demodemoApplication.class, args);
    }

}
